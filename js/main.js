import { Task } from "./model.js";

// import Task from "./model.js";
const BASE_URL = "https://62f8b755e0564480352bf426.mockapi.io/todo";

function renderList(listName){
    let todoHTML = "";
    let completedHTML = "";
    for (let index = 0; index < listName.length; index++){
        let content = `
            <li>
                ${listName[index].taskName}
                <div class="buttons">
                    <span onclick="deleteTask('${listName[index].id}')" class="remove"><i class="fa fa-trash-alt"></i></span>
                    <span onclick="markCompleted('${listName[index].id}')" class="complete"><i class="fa fa-check-circle"></i></span>
                </div>
            </li>
            `;
        if(listName[index].done == false){
            todoHTML+=content;
        } else {
            completedHTML+= content;
        }
    };
    document.getElementById("todo").innerHTML = todoHTML;
    document.getElementById("completed").innerHTML = completedHTML;
}


let getData = () => {
    axios({
        url:`${BASE_URL}`,
        method:"GET",
    })
    .then(function(res){
        // console.log(res.data);
        renderList(res.data);
    })
    .catch(function(err){
        console.log(err);
    })
}
window.getData = getData();
getData();


let addNewTask = () =>{
    let newTaskName = document.getElementById("newTask").value;
    if(newTaskName == null || newTaskName == ""){
        document.getElementById("newTask").value="";
    } else {
        let newTask = new Task(newTaskName,false,0)
        axios({
            url:`${BASE_URL}`,
            method:"POST",
            data:newTask,
        })
        .then(function(res){
            document.getElementById("newTask").value="";
            getData();
        })
        .catch(function(err){
            console.log(err);
        })
    }
}
window.addNewTask = addNewTask;

function deleteTask(id) {
    axios({
        url:`${BASE_URL}/${id}`,
        method:"DELETE",
    })
    .then(function(res){
        // console.log(res.data);
        getData();
    })
    .catch(function(err){
        console.log(err);
    })
}
window.deleteTask = deleteTask;

function markCompleted(id){
    axios({
        url:`${BASE_URL}/${id}`,
        method:"GET",
    })
    .then(function(res){
        let task = res.data;
        if (task.done == true){
            axios({
                url:`${BASE_URL}/${id}`,
                method:"PUT",
                data: {
                    done: false,
                }
            })
            .then(function(){
                getData();
            })
            .catch(function(err){
                console.log(err);
            })
        } else {
            axios({
                url:`${BASE_URL}/${id}`,
                method:"PUT",
                data: {
                    done: true ,
                }
            })
            .then(function(){
                getData();
            })
            .catch(function(err){
                console.log(err);
            })
        }
    })
    .catch(function(err){
        console.log(err);
    })
    
}
window.markCompleted = markCompleted;

function sortAZ() {
    axios({
        url:`${BASE_URL}?sortBy=taskName&order=asc`,
        method:"GET",
    })
    .then(function(res){
        // console.log(res.data);
        renderList(res.data);
    })
    .catch(function(err){
        console.log(err);
    })

}
window.sortAZ = sortAZ;

function sortZA() {
    axios({
        url:`${BASE_URL}?sortBy=taskName&order=desc`,
        method:"GET",
    })
    .then(function(res){
        // console.log(res.data);
        renderList(res.data);
    })
    .catch(function(err){
        console.log(err);
    })
}
window.sortZA = sortZA;